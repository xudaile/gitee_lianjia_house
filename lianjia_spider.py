import time
import re
import logging
import csv
import socket
from urllib import request, parse
from bs4 import BeautifulSoup
from database_operation import operate_record, operate_redis
from concurrent.futures import ThreadPoolExecutor, as_completed
from utils import array_util, position_util



logger = logging.getLogger()  # 不加名称设置root logger
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(threadName)s - %(levelname)s: - %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S')
# 使用FileHandler输出到文件
fh = logging.FileHandler('log.txt')
fh.setLevel(logging.INFO)
fh.setFormatter(formatter)
# 使用StreamHandler输出到屏幕
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch.setFormatter(formatter)
# 添加两个Handler
logger.addHandler(ch)
logger.addHandler(fh)


def get_list_url(url):
    agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36'
    headers = {'User-Agent': agent,
               'Host': 'xa.fang.lianjia.com'}
    req = request.Request(url, headers=headers)
    try:
        with request.urlopen(req, timeout=10) as res:
            res_str = res.read().decode("utf-8")
    except request.HTTPError as e:
        # 失败重新放回
        operate_redis.sadd_data('LJ_URL', [url])
        logger.info('----->HTTPError1:')
        logger.info(e)
        return None
    except request.URLError:
        # 失败重新放回
        operate_redis.sadd_data('LJ_URL', [url])
        return None
    except socket.timeout as e:
        # 失败重新放回
        operate_redis.sadd_data('LJ_URL', [url])
        logger.info(e)
        logger.info('--------->socket timeout')
        return None
    soup = BeautifulSoup(res_str, 'html.parser')
    li_all_soup = soup.find('ul', class_='resblock-list-wrapper').find_all('li')
    home_list = []
    for li_soup in li_all_soup:
        # 详情url
        href = li_soup.find('div', class_='resblock-name').find('a').get('href')
        # 名称
        name = li_soup.find('div', class_='resblock-name').find('a').get_text()
        # 地区
        region_one = li_soup.find('div', class_='resblock-location').find('span').get_text()
        region_two = li_soup.find('div', class_='resblock-location').find('span') \
            .next_sibling.next_sibling.next_sibling.next_sibling.get_text()
        region = region_one + region_two
        # 地址
        address = li_soup.find('div', class_='resblock-location').find('a').get_text()
        # 价格
        price = li_soup.find('div', class_='main-price').find('span', class_='number').get_text()
        home_jo = {'name': name, 'href': href, 'region': region, 'address': address,
                   'price': price}
        home_list.append(home_jo)
    print(home_list)
    # 批量新增
    batch_insert(home_list)


'''
表结构
create table lianjia_house
(
  id   serial not null
    primary key,
  name varchar(255),
  href varchar(200),
  address varchar(200),
  region varchar(200),      -- 地区
  price varchar(50),        -- 价格
  hand_time varchar(200),  -- 交房时间
  households varchar(100),  -- 户数
  cover_area varchar(200),  -- 占地面积
  develop_name varchar(100), -- 开发商名称
  loc varchar(250)
);
'''
def batch_insert(data_list):
    if len(data_list) < 1:
        return
    batch_insert_values = ''
    for i in range(len(data_list)):
        data = data_list[i]
        name = data['name']
        detail_href = data['href']
        address = data['address']
        region = data['region']
        price = data['price']
        batch_insert_values += "('" + name + "','" + detail_href + "','" + region + "','" + address + "','" + price + "')"
        if i < len(data_list) - 1:
            batch_insert_values += ','
    batch_insert_sql = 'insert into lianjia_house(name,href,region,address,price) values '
    batch_insert_sql += batch_insert_values
    print(batch_insert_sql)
    operate_record.save_record(batch_insert_sql)

# 设置URL到redis
def set_urls():
    url = "https://xa.fang.lianjia.com/loupan/pg{0}/"
    for i in range(1, 100+1):
        request_url = url.format(i)
        print(request_url)
        operate_redis.sadd_data('LJ_URL', [request_url])
    print('------->finished')


def get_list_data():
    request_url_list = operate_redis.spop_data('LJ_URL', 30)
    if len(request_url_list) < 1:
        print('-------无数据')
        return
    for request_url in request_url_list:
        url = str(request_url, encoding='utf8')
        print(url)
        get_list_url(url)
        print('----------------------')
    print('--------->finished')


# 获取详情数据
def get_detail_data(origin):
    database_id = origin[0]
    href = origin[1]
    agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36'
    headers = {'User-Agent': agent,
               'Host': 'xa.fang.lianjia.com'}
    url = 'https://xa.fang.lianjia.com' + href + 'xiangqing/'
    logger.info(url)
    req = request.Request(url, headers=headers)
    try:
        with request.urlopen(req, timeout=10) as res:
            res_str = res.read().decode("utf-8")
    except request.HTTPError as e:
        logger.info('----->HTTPError1:')
        logger.info(e)
        return None
    except request.URLError:
        return None
    except socket.timeout as e:
        logger.info(e)
        logger.info('--------->socket timeout')
        return None
    soup = BeautifulSoup(res_str, 'html.parser')
    # 交房时间
    hand_time_soup = soup.find('span', class_='fq-td fq-open')
    hand_time = ''
    if hand_time_soup is not None:
        hand_time = str(hand_time_soup.get_text()).strip()
    box_all_soup = soup.find_all('ul', class_='x-box')
    li_all_soup = box_all_soup[1].find_all('li')
    house_jo = {}
    for li_soup in li_all_soup:
        key = li_soup.find('span', class_='label').get_text()
        key = str(key).strip()
        value = li_soup.find('span', class_='label-val').get_text()
        value = str(value).strip()
        house_jo[key] = value
    households = cover_area = ''
    if '规划户数：' in house_jo.keys():
        households = house_jo['规划户数：']
    if '占地面积：' in house_jo.keys():
        cover_area = house_jo['占地面积：']
    script = soup.find_all("script")
    pattern = re.compile(r"\"longitude\":\"(.*?)\",\"latitude\":\"(.*?)\"")
    lng_lat_list = pattern.findall(str(script))
    loc = lng_lat_list[0][0] + ',' + lng_lat_list[0][1]
    home_jo = {'database_id': database_id, 'hand_time': hand_time, 'households': households,
               'cover_area': cover_area, 'loc': loc}
    return home_jo


# {'hand_time': '2019-6-30', 'households': '471', 'cover_area': '11,200㎡', 'loc': '108.889214,34.257773'}
def batch_data(data_list):
    if len(data_list) < 1:
        return
    batch_update_values = ''
    for i in range(len(data_list)):
        data = data_list[i]
        database_id = data['database_id']
        hand_time = data['hand_time']
        households = data['households']
        cover_area = data['cover_area']
        loc = data['loc']
        lon_lat_list = loc.split(',')
        location = position_util.bd09_to_gps84(float(lon_lat_list[0]), float(lon_lat_list[1]))
        point_geom = str(location[0]) + ' ' + str(location[1])
        point_geom = "ST_GeomFromText('Point(" + point_geom + ")',4326)"
        batch_update_values += "(" + str(database_id) + ",'" + households + "','" + hand_time + "','" + cover_area \
                               + "','" + loc + "'," + point_geom + ")"
        if i < len(data_list) - 1:
            batch_update_values += ','
    batch_update_sql = "update lianjia_house set is_success=true,households=tmp.households,hand_time=tmp.hand_time," \
                       "cover_area=tmp.cover_area,loc=tmp.loc,location=tmp.point_geom " \
                       "from (values {0}) as tmp (id,households,hand_time,cover_area,loc,point_geom) " \
                       "where lianjia_house.id=tmp.id"
    batch_update_sql = batch_update_sql.format(batch_update_values)
    print(batch_update_sql)
    operate_record.save_record(batch_update_sql)


# 获取详情数据
def get_house_detail():
    sql = 'select t.id,t.href from lianjia_house t where t.is_success=false order by t.id asc limit 500 '
    rows = operate_record.query_record_list(sql)
    if len(rows) == 0:
        print('-------暂无数据')
        return None
    group_list = array_util.list_split_by_size(len(rows), 20)
    if not group_list:
        print('-------暂无数据')
        return
    print(group_list)
    for s in group_list:
        sts = s.split(",", 2)
        thread_list = rows[int(sts[0]):int(sts[1]) + 1]
        all_task = []
        executor = ThreadPoolExecutor(max_workers=len(thread_list), thread_name_prefix='ThreadPool')
        for orgin in thread_list:
            # 多线程处理
            task = executor.submit(get_detail_data, (orgin[0], orgin[1]))
            all_task.append(task)
        # 等待全部执行完成
        data_list = []
        for task in as_completed(all_task):
            result = task.result()
            if not result:
                continue
            data_list.append(result)
        print(data_list)
        # 处理数据
        batch_data(data_list)
        # 关闭线程
        executor.shutdown(wait=True)
        # 等一会
        time.sleep(1)
    print('------>{ finished }')


def export_csv():
    sql = "select t.id,t.name,t.households,t.hand_time,t.price,t.city,t.region " \
          "from lianjia_house t where t.city ='{0}'"
    sql = sql.format('西安市')
    rows = operate_record.query_record_list(sql)
    if len(rows) == 0:
        print('-------暂无数据')
        return
    headers = ['数据唯一ID', '名称', '户数', '开盘时间', '价格', '城市', '地区']
    file_path = '/Users/bizvane_xdl/execl/陕西/西安链家新房数据.csv'
    with open(file_path, 'w', encoding='GB2312') as f:
        f_csv = csv.writer(f)
        f_csv.writerow(headers)
        for row in rows:
            row_jo = row
            print(row_jo)
            f_csv.writerow(row_jo)
    print('------>finished')

def main():
    # 1.设置链接
    # set_urls()
    # 2.从redis获取url
    # get_list_data()
    # 3.获取详情数据
    # get_house_detail()
    # 4.导出csv
    export_csv()


if __name__ == '__main__':
    main()