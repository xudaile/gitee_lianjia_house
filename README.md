# 链家新房数据爬取

#### 介绍
针对链家网数据爬取，主要获取新房数据。

#### 爬虫说明
1.根据链接爬取列表数据（如：名称，详细链接）。<br>
2.根据详细链接获取详情数据。（如：户数，开盘时间，地址，坐标，等）<br>
3.保存数据到数据库。<br>
4.导出csv。<br>

### 技术应用
1.request 原生请求。<br>
2.psycopg2 连接数据库保存数据。<br>
3.BeautifulSoup 定位页面元素信息。<br>
4.ThreadPoolExecutor 多线程并发请求<br>
5.csv 导出csv <br>

### 示例图
<p>链家新房网页图</p>
<div style="display:flex; height:auto; justify-content: center;">
<img src="https://gitee.com/xudaile/gitee_lianjia_house/raw/master/images/lianjia.jpg" width="800px"/>
</div>
<p>导出csv图</p>
<div style="display:flex; height:auto; justify-content: center;">
<img src="https://gitee.com/xudaile/gitee_lianjia_house/raw/master/images/lianjia_one.jpg" width="800px"/>
</div>