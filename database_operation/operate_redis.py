import redis
pool = redis.ConnectionPool(host='127.0.0.1', port=6379, db=5)
r = redis.Redis(connection_pool=pool)


def sadd_data(key, value_list):
    for value in value_list:
        r.sadd(key, value)


def spop_data(key, number):
    return r.spop(key, number)

def smember_list(key):
    return r.smembers(key)


def scard_count(key):
    return r.scard(key)


# 列表左边取一个
def lpop_data(key):
    return r.lpop(key)


# 列表从右取一个
def rpop_data(key):
    return r.rpop(key)


def lpush_data(key, value_list):
    for value in value_list:
        r.lpush(key, value)


def llen_count(key):
    return r.llen(key)


def delete_key(key):
    r.delete(key)

if __name__ == '__main__':
    delete_key('IP')
    # lpush_data('IP', ["223.241.116.121:8010", "223.241.116.122:8010"])
    # print(llen_count('IP'))
    # print(str(lpop_data('IP'), encoding="utf-8"))
    #